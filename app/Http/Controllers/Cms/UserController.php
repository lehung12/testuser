<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    private $user;
    /**
     * UserController constructor.
     */
    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        $user  = $this->user->getAll();
        $response = [
            "title" => "DASHBOARD",
            "user" => $user,
        ];
        return view('admin.dashboard', $response);
    }
    public function list()
    {
        $list_users = $this->user->getAll();
        return view('admin.user.index', compact('list_users'));
    }
    public function create()
    {
        return view('admin.user.add');
    }
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'email' => 'unique:users,email',
                'password' => 'max:8 | min:6'
            ],
            [
                'email.unique' => 'Email đã tồn tại !',
                'password.max' => 'Mật khẩu tối đa 8 ký tự !',
                'password.min' => 'Mật khẩu tối thiểu 6 ký tự !',
            ]);
//        dd($request->all());
        $data = $request->all();
        $data['password'] = bcrypt($request->password);
        $this->user->create($data);
        return redirect()->route('listUser')->with('notify', 'Thêm mới tài khoản thành công !');
    }
    public function edit($id)
    {
        $user = $this->user->getById($id);
        return view('admin.user.edit', compact('user'));
    }
    public function update($id, Request $request)
    {
        $this->user->update($id, $request->all());
        return redirect()->route('listUser')->with('notify','Cập nhật thông tin thành công !');
    }
    public function delete($id)
    {
        $this->user->delete($id);
        return redirect()->back()->with('notify','Xóa tài khoản thành công !');
    }
    public function changePassword($id,Request $request){
        $this->user->update($id, ['password' => bcrypt($request->password)]);
        return redirect()->route('listUser')->with('notify','Cập nhật mật khẩu thành công !');

    }
}
