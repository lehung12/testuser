<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    //
    public function getLogin()
    {
        if (Auth::check()) {
            return Redirect::to("/admin");
        } else {
            return view('admin.login');
        }
    }
    public function postLogin(Request $request){
        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password])){
            $user = User::where('email',$request->email)->first();
            $request->session()->put('user',$user);
            return redirect(route('homeAdmin'));
        }else{
            return redirect(route('loginAdmin'))->with('error','Tài khoản hoặc mật khẩu không chính xác !');
        }
    }
    public function getLogout(Request $request){
        if ($request->session()->has('user')) {
            $request->session()->forget('user');
        }
        $request->session()->flush();
        return redirect(route('loginAdmin'))->with('error','Bạn đã thoát khỏi hệ thống');
    }
}
