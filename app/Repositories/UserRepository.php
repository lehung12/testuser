<?php
namespace App\Repositories;
interface UserRepository{
    public function getAll();

    public function getById($id);

    public function getByColumn($att, $column);

    public function create(array $att);

    public function update($id, array $att);

    public function delete($id);
}