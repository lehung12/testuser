<?php
namespace App\Repositories;

use App\User;

class EloquentUser implements UserRepository
{
    private $model;
    /**
     * EloquentUser constructor.
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
        return $this->model->orderBy('level', 'DESC')->paginate(10);
    }

    public function getById($id)
    {
        // TODO: Implement getById() method.
        return $this->model->find($id);
    }

    public function getByColumn($att, $column)
    {
        // TODO: Implement getByColumn() method.
    }

    public function create(array $att)
    {
        // TODO: Implement create() method.
        return $this->model->create($att);
    }

    public function update($id, array $att)
    {
        // TODO: Implement update() method.
        $user = $this->model->findOrFail($id);
        $user->update($att);
        return $user;
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
        $this->model->find($id)->delete();
        return true;
    }
}