<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/login', 'Cms\LoginController@getLogin')->name('loginAdmin');
Route::post('/login', 'Cms\LoginController@postLogin')->name('postLogin');
Route::get('/logout', 'Cms\LoginController@getLogout')->name('getLogout');
Route::group(['middleware' => 'admin_auth'], function () {
    Route::get('/admin', 'Cms\UserController@index')->name('homeAdmin');
    Route::group(['prefix' => 'user'], function () {
        Route::get('/', "Cms\UserController@list")->name('listUser');
        Route::get('/create', "Cms\UserController@create");
        Route::post('/store', "Cms\UserController@store");
        Route::get('/edit/{id}', "Cms\UserController@edit");
        Route::post('/update/{id}', "Cms\UserController@update");
        Route::post('/change-password/{id}', "Cms\UserController@changePassword");
        Route::get('/delete/{id}', "Cms\UserController@delete");
    });
});
