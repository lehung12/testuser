@extends("admin.layout.master")

@section("content")
    <h1></h1>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> Cập nhật thông tin</span>
                    </div>
                    <div class="actions">
                        <a href="{{URL::action("Cms\UserController@list")}}" class="btn btn-default">
                            Quay lại <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{URL::action("Cms\UserController@update", $user->id)}}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Email</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email"
                                       value="{{ $user->email }}" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Tên</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Quyền
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-6">
                                <select class="form-control" name="level" required>
                                    <option value="">Chọn quyền</option>
                                    <option value="1" @if($user->level == 1) selected @endif>Người dùng</option>
                                    <option value="2" @if($user->level == 2) selected @endif>Quản trị</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn green">
                                    Chỉnh sửa
                                </button>
                            </div>
                        </div>
                    </form>
                    <button type="button" class="btn blue" data-toggle="modal" data-target="#myModal">Đổi mật khẩu</button>
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Đổi mật khẩu</h4>
                                </div>
                                <form action="{{URL::action("Cms\UserController@changePassword", $user->id)}}" method="post">
                                    {{csrf_field()}}
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label class="control-label">Nhập mật khẩu mới
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="password" class="form-control pass1" name="password" required/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Nhập mật lại mật khẩu mới
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="password" class="form-control pass2" required/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                        <button type="submit" class="btn btn-success">Đổi</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".pass2").change(function () {
                if($(this).val() != $(".pass1").val()){
                    alert("Nhập lại mật khẩu không trùng khớp");
                    $(this).val("");
                }
            });
        });
    </script>
@endsection
