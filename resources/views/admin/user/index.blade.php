@extends("admin.layout.master")
@section("styles")

@endsection
@section("content")
    {{--<h1 class="page-title"> {{$title}}--}}
        {{--<small>{{$title_description}}</small>--}}
    {{--</h1>--}}

    <div class="row">
        <div class="col-md-12">
            @if (session('notify'))
                <div class="alert alert-success">
                    {{ session('notify') }}
                </div>
        @endif
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> Danh sách tài khoản</span>
                    </div>
                    <div class="actions">
                        <a href="{{URL::action("Cms\UserController@create")}}" class="btn btn-success">
                            <i class="fa fa-plus"></i> Thêm mới
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-middle table-striped table-condensed flip-content">
                        <thead class="flip-content">
                        <tr>
                            <th> STT</th>
                            <th> Tên</th>
                            <th> Email</th>
                            <th> Quyền</th>
                            <th colspan="2"> Tùy chọn</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list_users as $key => $item)
                            <tr>
                                <td width="50px">{{($list_users->currentPage()-1)*$list_users->perPage()+$key+1}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->email}}</td>
                                <td>
                                    @if($item->level == 2)
                                        <button class="btn btn-circle btn-xs green change-status">Người quản trị</button>
                                    @else
                                        <button class="btn btn-circle btn-xs default change-status">Người dùng</button>
                                    @endif
                                </td>
                                <td class="numeric" width="50px">
                                    <a href="{{ URL::action("Cms\UserController@edit", $item->id) }}"
                                       class="btn yellow-gold" title="Sửa"> <i class="fa fa-edit"></i></a>
                                </td>
                                <td class="numeric" width="50px">
                                    <a href="{{ URL::action("Cms\UserController@delete", $item->id) }}"
                                    onclick="return confirm('Xác nhận xóa?')"
                                       class="btn red"><i class="fa fa-trash" title="Xóa"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div align="center">
                        {{$list_users->links()}}
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
@endsection