@extends("admin.layout.master")

@section("content")
    <h1></h1>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> Thêm mới tài khoản</span>
                    </div>
                    <div class="actions">
                        <a href="{{URL::action("Cms\UserController@list")}}" class="btn btn-default">
                            Quay lại <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{URL::action("Cms\UserController@store")}}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Tên <span class="required"> * </span></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                            </div>
                        </div>

                        {{--<div class="form-group">--}}
                            {{--<label for="email" class="col-md-4 control-label">Tên đăng nhập <span class="required"> * </span></label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="text" class="form-control" name="username"--}}
                                       {{--value="{{ old('username') }}">--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">E-Mail <span class="required"> * </span></label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email"
                                       value="{{ old('email') }}" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Password <span class="required"> * </span></label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control password" name="password" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password <span class="required"> * </span></label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control password2"
                                       name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4">Quyền
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-6">
                                <select class="form-control" name="level" required>
                                    <option value="">Chọn quyền</option>
                                    <option value="1" @if(old('level')== 1) selected @endif>Người dùng</option>
                                    <option value="2" @if(old('level')== 2) selected @endif>Quản trị</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn green">
                                    Thêm mới
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".password2").change(function () {
                if($(this).val() != $(".password").val()){
                    alert("Nhập lại mật khẩu không trùng khớp");
                    $(this).val("");
                }
            });
        });
    </script>
@endsection
